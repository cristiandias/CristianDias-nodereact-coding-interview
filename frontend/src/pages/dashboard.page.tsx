import React, { FC, useState, useEffect } from "react";

import { RouteComponentProps } from "@reach/router";
import { IUserProps } from "../dtos/user.dto";
import { UserCard } from "../components/users/user-card";
import { CircularProgress, TextField, Button } from "@mui/material";

import { BackendClient } from "../clients/backend.client";

const backendClient = new BackendClient();

enum RequestStatus {
  LOADING,
  SUCCESS,
  ERROR,
}

export const DashboardPage: FC<RouteComponentProps> = () => {
  const [users, setUsers] = useState<IUserProps[]>([]);
  const [requestStatus, setRequestStatus] = useState(RequestStatus.LOADING);

  const [search, setSearch] = useState('');
  const [searchQuery, setSearchQuery] = useState('');

  useEffect(() => {
    const fetchData = async () => {
      setRequestStatus(RequestStatus.LOADING);
      try {
        const result = await backendClient.getAllUsers(searchQuery);
        setUsers(result.data);
        setRequestStatus(RequestStatus.SUCCESS);
      } catch (error) {
        setRequestStatus(RequestStatus.ERROR);
      }
    };

    fetchData();
  }, [searchQuery]);

  const handleChangeSearch = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSearch(event.target.value);
  };

  const handleClickSearch = () => {
    setSearchQuery(search);
  }

  if (requestStatus === RequestStatus.ERROR) {
    return <div>ERROR</div>;
  }

  return (
    <div style={{ paddingTop: "30px" }}>
      <div>
        <TextField id="search" label="Outlined" variant="outlined" value={search} onChange={handleChangeSearch} />
        <Button onClick={handleClickSearch}>Search</Button>
      </div>

      <div style={{ display: "flex", justifyContent: "space-between" }}>
        {requestStatus === RequestStatus.LOADING ? (
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              height: "100vh",
            }}
          >
            <CircularProgress size="60px" />
          </div>
        ) : (
          <div>
            {users.length
              ? users.map((user) => {
                return <UserCard key={user.id} {...user} />;
              })
              : null}
          </div>
        )}
      </div>
    </div>
  );
};
