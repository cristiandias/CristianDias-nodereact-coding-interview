import people_data from '../data/people_data.json';

export class PeopleProcessing {
    getById(id: number) {
        return people_data.find((p) => p.id === id);
    }

    getAll(searchTerm?: string) {
        return searchTerm 
            ? people_data.filter(people =>
                people.first_name.includes(searchTerm) ||
                people.last_name.includes(searchTerm) ||
                people.company.includes(searchTerm) ||
                people.title?.includes(searchTerm)
            ) : people_data;
    }
}
